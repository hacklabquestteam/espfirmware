#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include "FS.h"

const byte DNS_PORT = 53;
IPAddress apIP(192, 168, 1, 1);
DNSServer dnsServer;
ESP8266WebServer webServer(80);


char *buffer;
int size;

void setup() {
  Serial.begin(115200);
  Serial.println("Started");
  Serial.println("Opening file");

  SPIFFS.begin();
  File file = SPIFFS.open("/index.html", "r");
  if (!file) {
    Serial.println("Error: file open failed");
    return;
  }

  Serial.print("File found ");
  size = file.size();
  Serial.println(size);

  buffer = (char *) malloc(size);
  file.readBytes(buffer, size);
  file.close();
  
  SPIFFS.end();

  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
  WiFi.softAP("HacklabQuest");

  // if DNSServer is started with "*" for domain name, it will reply with
  // provided IP to all DNS request
  dnsServer.start(DNS_PORT, "*", apIP);

  // replay to all requests with same HTML
  webServer.onNotFound([]() {
    webServer.send(200, "text/html", buffer);
  });
  webServer.begin();
}

void loop() {
  dnsServer.processNextRequest();
  webServer.handleClient();
}
